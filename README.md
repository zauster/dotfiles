# Dotfiles

Uses GNU stow

## \$HOME dotfiles

E.g. `bash` dotfiles can be symlinked to `/home/user` with: 

``` sh
stow bash
```

## /etc/ dotfiles

Use this command for stowing symlinks for config files in `/etc`.

``` sh
sudo stow -t / etc
```

## systemd user units

systemd user units have to be started afterwards with

``` sh
systemctl --user enable hibernate_on_low_battery.timer
systemctl --user start hibernate_on_low_battery.timer
```

For the `mbsync` timers, enable the `timers.target`:

``` sh
systemctl --user enable timers.target
systemctl --user enable mbsync_gmail.timer mbsync_snapdragon.timer mbsync_wiiw.timer
systemctl --user start mbsync_gmail.timer mbsync_snapdragon.timer mbsync_wiiw.timer
```

