#!/bin/sh

## Setting this, so the repo does not need to be given on the commandline:
## export BORG_REPO=ssh://reitero@rpi:/mnt/backup_wd/backup_Arbeit
# export BORG_REPO=reitero@rpi:/mnt/backup_tosh/backup_Arbeit
# export BORG_REPO=reitero@rpi:/mnt/backup_wd/backup_Arbeit

## use GPG to get the passphrase contained in a gpg-encrypted file:
# export BORG_PASSCOMMAND='gpg2 --quiet --for-your-eyes-only --no-tty --decrypt /home/reitero/.config/borg/borg-toshiba-passphrase.gpg'
# export BORG_PASSCOMMAND='gpg2 --quiet --for-your-eyes-only --no-tty --decrypt /home/reitero/.config/borg/borg-wd-passphrase.gpg'

## some helpers and error handling:
info() { printf "==> [%s] %s \n" "$( date )" "$*" >&2; }
trap 'echo $( date ) Backup interrupted >&2; exit 2' INT TERM

info "Starting backup"

## Backup the most important directories into an archive named after
## the machine this script is currently running on:

## % borg help patterns

## These patterns use a variant of shell pattern syntax,
## with '*' matching any number of characters, '?' matching
## any single character, '[...]' matching any single
## character specified, including ranges, and '[!...]'
## matching any character not specified. For the purpose of
## these patterns, the path separator (backslash for Windows
## and '/' on other systems) is not treated specially. Wrap
## meta-characters in brackets for a literal match (i.e. [?]
## to match the literal character ?). For a path to match a
## pattern, the full path must match, or it must match from
## the start of the full path to just before a path
## separator. Except for the root path, paths will never end
## in the path separator when matching is attempted. Thus,
## if a given pattern ends in a path separator, a '*' is
## appended before matching is attempted.
    # --verbose                                                 \

borg create                                                   \
    --filter AME                                              \
    --list                                                    \
    --stats                                                   \
    --show-rc                                                 \
    --compression lz4                                         \
    --exclude-caches                                          \
    --exclude '*.pyc'                                         \
    --exclude '*.mathml'                                      \
    --exclude '*.parquet'                                     \
    --exclude '*.git/'                                        \
    --exclude '/home/reitero/Arbeit/mnt'                      \
    --exclude '*/2022_Bertelsmann_SovereignEurope/data/*.txt' \
    --exclude '*/2022_Bertelsmann_SovereignEurope/data/*.zip' \
                                                              \
    ::'{hostname}-{now}'                                      \
    /home/reitero/Arbeit                                      \
    /home/reitero/Dropbox                                     \

backup_exit=$?

info "Pruning repository"

## Use the `prune` subcommand to maintain 7 daily, 4 weekly and 6 monthly
## archives of THIS machine. The '{hostname}-' prefix is very important to
## limit prune's operation to this machine's archives and not apply to
## other machines' archives also:

borg prune                          \
    --list                          \
    --prefix '{hostname}-'          \
    --show-rc                       \
    --keep-daily    7               \
    --keep-weekly   4               \
    --keep-monthly  6               \
    --keep-yearly   1               \

prune_exit=$?

## use highest exit code as global exit code
global_exit=$(( backup_exit > prune_exit ? backup_exit : prune_exit ))

if [ ${global_exit} -eq 0 ]; then
    info "Backup and Prune finished successfully"
elif [ ${global_exit} -eq 1 ]; then
    info "Backup and/or Prune finished with warnings"
else
    info "Backup and/or Prune finished with errors"
fi

exit ${global_exit}
