# git clone https://gitlab.archlinux.org/archlinux/packaging/packages/r.git
cd r
git checkout -- .
git pull
patch PKGBUILD < ../r-olreit.patch
makepkg -fsri

cd ..

# See https://www.vultr.com/docs/building-packages-on-arch-linux/
# git clone --single-branch --branch "packages/r" https://github.com/archlinux/svntogit-community.git "r"

## pacins r-3.5.2-2-x86_64.pkg.tar.xz --overwrite /usr/lib/R/lib/libRblas.so --overwrite /usr/lib/R/lib/libRlapack.so
## relinkRBLAS
