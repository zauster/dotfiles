;;; ../../dotfiles/doom/.config/doom/+julia.el -*- lexical-binding: t; -*-

;; Automatically indent the code when typing `end`.
(set-electric! 'julia-mode :words '("end"))

;; https://discourse.julialang.org/t/emacs-based-workflow/19400/73?page=4
(setq
 ;; lsp-julia-default-project "~/.julia/environments/v1.10"
 ;; eglot-jl-language-server-project "~/.julia/environments/v1.10"
 eglot-connect-timeout 5000)

(add-hook! 'julia-mode-hook
  (setq fill-column 80))

(after! julia-snail
  (set-popup-rule! "^\\*julia.*\\*$" ;; "^\\*julia wilap\\*$"
    :quit nil
    :side 'right
    :width .5
    :slot -1
    ;; :vslot -4
    :vslot 0
    :ttl nil
    :modeline t)

  (customize-set-variable 'split-height-threshold 15)
  (setq! julia-snail-repl-display-eval-results nil)
  (setq enable-remote-dir-locals t)

  (map! :map julia-snail-mode-map
        :localleader
        "," #'julia-snail-send-dwim
        ;; "t" #'julia-repl-includet-buffer
        ;; "b" #'julia-repl-send-buffer
        ;; "p" #'zauster--julia-repl-send-paragraph
        ;; "r" #'zauster--julia-repl-restart
        ;; "e" #'zauster--julia-repl-restart-and-includet
        )
  )

;; (after! julia-repl

  ;; (julia-repl-set-terminal-backend 'vterm)
  ;; (setq julia-repl-skip-comments t)

  ;; ;; copied from https://github.com/tpapp/julia-repl/issues/114
  ;; (defun zauster--julia-repl-send-paragraph ()
  ;;   (interactive)
  ;;   (save-mark-and-excursion
  ;;     (mark-paragraph)
  ;;     (julia-repl-send-region-or-line))
  ;;   (forward-paragraph))

  ;; (defun zauster--julia-repl-restart ()
  ;;   (interactive)
  ;;   (julia-repl--send-string "exit()")
  ;;   ;; (when (buffer-live-p julia-repl--script-buffer)
  ;;   ;;   (switch-to-buffer-other-window julia-repl--script-buffer))
  ;;   (sleep-for 0.7)
  ;;   (message "Restarting the julia REPL...")
  ;;   (julia-repl)
  ;;   (julia-repl--switch-back)
  ;;   )

  ;; (defun zauster--julia-repl-restart-and-includet ()
  ;;   (interactive)
  ;;   (zauster--julia-repl-restart)
  ;;   (julia-repl-includet-buffer)
  ;;   )

  ;; (map! :map julia-repl-mode-map
  ;;       :localleader
  ;;       "l" #'julia-repl-send-region-or-line
  ;;       "t" #'julia-repl-includet-buffer
  ;;       "b" #'julia-repl-send-buffer
  ;;       "p" #'zauster--julia-repl-send-paragraph
  ;;       "r" #'zauster--julia-repl-restart
  ;;       "e" #'zauster--julia-repl-restart-and-includet
  ;;       )

  ;; (map! :map julia-repl-mode-map
  ;;       :n [C-return] #'julia-repl-send-region-or-line
  ;;       )

  ;; (set-popup-rule! "^\\*julia:*.*\\*$"
  ;;   :quit nil
  ;;   :side 'right
  ;;   :width .5
  ;;   :slot 1
  ;;   :vslot -1
  ;;   :ttl nil)
  ;; )
