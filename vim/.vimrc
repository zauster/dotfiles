noremap h l
noremap l h

:filetype plugin on
:syntax on

:filetype indent on

:set relativenumber
