;;; package --- mu4e configuration
;;; Commentary: this file contains the configuration for mu4e.

(add-to-list 'load-path "/usr/share/emacs/site-lisp/mu4e")
(setq +mu4e-mu4e-mail-path "~/Mail/")

(after! mu4e

  ;;------------------------------------------------------------
  ;; Set up the contexts/accounts
  (set-email-account! "wiiw"
                      '((mu4e-sent-folder       . "/wiiw/Sent Items")
                        (mu4e-drafts-folder     . "/wiiw/Drafts")
                        (mu4e-trash-folder      . "/wiiw/Deleted Items")
                        (mu4e-refile-folder     . "/wiiw/Archive")
                        (smtpmail-smtp-user     . "reiter@wiiw.ac.at")
                        (user-full-name         . "Oliver Reiter")
                        (user-mail-address      . "reiter@wiiw.ac.at")
                        ;; (mu4e-compose-signature . "Oliver Reiter, wiiw")
                        )
                      t)

  (set-email-account! "gmail"
                      '((mu4e-sent-folder       . "/gmail/[Gmail]/Gesendet")
                        (mu4e-drafts-folder     . "/gmail/[Gmail]/Entwürfe")
                        (mu4e-trash-folder      . "/gmail/[Gmail]/Papierkorb")
                        (mu4e-refile-folder     . "/gmail/Archive")
                        (smtpmail-smtp-user     . "revilo.retier@gmail.com")
                        (user-full-name         . "Oliver Reiter")
                        (user-mail-address      . "revilo.retier@gmail.com")
                        ;; (mu4e-compose-signature . "Oliver Reiter, gmail")
                        )
                      nil)

  (set-email-account! "snapdragon"
                      '((mu4e-sent-folder       . "/snapdragon/Sent Messages")
                        (mu4e-drafts-folder     . "/snapdragon/Drafts")
                        (mu4e-trash-folder      . "/snapdragon/Deleted Messages")
                        (mu4e-refile-folder     . "/snapdragon/Archive")
                        (smtpmail-smtp-user     . "oliver.reiter@snapdragon.cc")
                        (user-full-name         . "Oliver Reiter")
                        (user-mail-address      . "oliver.reiter@snapdragon.cc")
                        ;; (mu4e-compose-signature . "Oliver Reiter, snapdragon")
                        )
                      nil)

  (setq +mu4e-gmail-accounts '(("revilo.retier@gmail.com" . "gmail")))


  ;;------------------------------------------------------------
  ;; Bookmarks
  (setq mu4e-bookmarks
        '(
          ( :name "Work: Today's messages"
            :query "date:today..now AND maildir:/wiiw/ AND NOT \"maildir:/wiiw/Deleted Items\" AND NOT maildir:/wiiw/Newsletter AND NOT \"maildir:/wiiw/Junk E-mail\""
            :key ?t)
          ( :name "Work: Last 7 days"
            :query "date:7d..now AND maildir:/wiiw/ AND NOT \"maildir:/wiiw/Deleted Items\" AND NOT maildir:/wiiw/Newsletter AND NOT \"maildir:/wiiw/Junk E-mail\""
            :key ?w)
          ( :name  "Work: Robert Stehrer"
            :query "date:28d..now AND maildir:/wiiw/ AND (from:stehrer OR to:stehrer) AND NOT \"maildir:/wiiw/Deleted Items\" AND NOT \"maildir:/wiiw/Junk E-mail\""
            :key ?r)
          ( :name  "Work: Bernhard Schütz"
            :query "date:28d..now AND maildir:/wiiw/ AND (from:schütz OR from:schuetz OR to:schütz OR to:schuetz) AND NOT \"maildir:/wiiw/Deleted Items\" AND NOT \"maildir:/wiiw/Junk E-mail\""
            :key ?a)
          ( :name  "Work: Sandra Leitner"
            :query "date:28d..now AND maildir:/wiiw/ AND (from:\"Sandra Leitner\" OR to:\"Sandra Leitner\" AND NOT \"maildir:/wiiw/Deleted Items\" AND NOT \"maildir:/wiiw/Junk E-mail\""
            :key ?l)
          ( :name "Work: Newsletters"
            :query "maildir:/wiiw/Newsletter"
            :key ?n)
          ;; `( :name  "Work: From Who?"
          ;;   :query ,(lambda () (call-interactively 'zauster--mu4e-bookmark-ask-for-person-query))
          ;;   :key ?f)
          ( :name "Gmail: Last 7 days"
            :query "maildir:/gmail/ AND NOT \"maildir:/gmail/[Gmail]/Papierkorb\""
            :key ?g)
          ( :name "Snapdragon: Last 7 days"
            :query "maildir:/snapdragon/ AND NOT \"maildir:/snapdragon/Deleted Messages\" AND NOT \"maildir:/snapdragon/Junk\""
            :key ?s)
          ( :name "All: Messages with images"
            :query "mime:image/*"
            :key ?p)
          ( :name  "All: Big messages"
            :query "size:5M..500M"
            :key ?b)
          ( :name  "All: Unread messages"
            :query "flag:unread AND NOT flag:trashed"
            :key ?u)
          ))

  ;;------------------------------------------------------------
  ;; Add a new mark: Mark-as-read-and-delete for messages
  ;; https://github.com/djcb/mu/issues/2671
  (add-to-list 'mu4e-marks
               '(mark-as-read-and-delete
                 :char       ("A" . "🗹")
                 :prompt     "mark-as-read-and-delete"
                 :show-target (lambda (target) "read+delete")
                 ;; :dyn-target (lambda (target msg) (mu4e-get-refile-folder msg))
                 :action      (lambda (docid msg target)
                                (mu4e--server-move docid nil "+S-u-N") ;; mark as read
                                (mu4e--server-remove docid) ;; delete
                                )))

  (defun zauster--mu4e-mark-as-read-and-delete ()
    "Mark message at point as read and delete it."
    (interactive)
    (mu4e-headers-mark-and-next 'mark-as-read-and-delete))

  (define-key mu4e-headers-mode-map (kbd "ü") #'zauster--mu4e-mark-as-read-and-delete)

  ;;------------------------------------------------------------
  ;; 1. Getting the mail
  (setq
   ;; mu4e-get-mail-command "mbsync -c ~/.config/mail/.mbsyncrc --all | tee -a ~/.cache/mbsync.log" ;; -V for verbosity
   ;; mu4e-get-mail-command "mbsync -c ~/.config/mail/.mbsyncrc --all"
   mu4e-get-mail-command "true"
   mu4e-update-interval  (* 60 2) ;; every second minute
   mu4e-main-hide-personal-addresses t
   )

  ;;------------------------------------------------------------
  ;; 2. Indexing it
  ;; Speeding up indexing
  ;; disabled, because counts of unread mails do not get
  ;; updated often enough, which is annoying
  (setq
   mu4e-index-cleanup t ;; nil      ;; don't do a full cleanup check
   mu4e-index-lazy-check nil) ;;t)    ;; don't consider up-to-date dirs

  ;; set 'U' to nonlazy updating if counts get out of sync
  (map!
   :after mu4e
   :map mu4e-main-mode-map
   :n "g u" #'mu4e-update-index-nonlazy
   :map mu4e-headers-mode-map
   :n "g u" #'mu4e-update-index-nonlazy)

  ;;------------------------------------------------------------
  ;; 2a. Viewing the mail
  (setq
   mu4e-view-use-old nil ;; experimental, but usable (apparently)
   gnus-blocked-images "http" ;; or ".", block all (external) images
   )

  ;;------------------------------------------------------------
  ;; 3. Sending mail
  (setq
   sendmail-program (executable-find "msmtp")
   send-mail-function 'smtpmail-send-it
   message-sendmail-f-is-evil t
   message-sendmail-extra-arguments '("--read-envelope-from")
   message-send-mail-function 'message-send-mail-with-sendmail
   )

  ;;------------------------------------------------------------
  ;; mu4e decryption
  (setq mu4e-decryption-policy 'ask)

  (add-hook 'mu4e-compose-mode-hook
            #'(lambda ()
                (auto-save-mode -1)
                ;; (org-msg-mode)
                ;; (use-hard-newlines -1)
                ;; (setq mu4e-compose-format-flowed nil)
                ))

  ;; Copied from: https://www.djcbsoftware.nl/code/mu/mu4e/Compose-hooks.html
  ;; 1) messages to me@foo.example.com (in to/from or cc
  ;; fields) should be replied with From:me@foo.example.com
  ;; 2) messages to me@bar.example.com (in to/from or cc
  ;; fields) should be replied with From:me@bar.example.com
  ;; 3) all other mail should use From:me@cuux.example.com
  (add-hook 'mu4e-compose-pre-hook
            (defun my-set-from-address ()
              "Set the From address based on the To address of the original."
              (let ((msg mu4e-compose-parent-message)) ;; msg is shorter...
                (when msg
                  (setq user-mail-address
                        (cond
                         ((string-prefix-p "/wiiw" (mu4e-message-field msg :maildir))
                          "reiter@wiiw.ac.at")
                         ((or (mu4e-message-contact-field-matches msg :to "oliver.reiter@snapdragon.cc")
                              (or (mu4e-message-contact-field-matches msg :from "oliver.reiter@snapdragon.cc")
                                  (mu4e-message-contact-field-matches msg :cc "oliver.reiter@snapdragon.cc")))
                          "oliver.reiter@snapdragon.cc")
                         (t "oliver_reiter@gmx.at")))))))

  ;;------------------------------------------------------------
  ;; Other settings
  (setq
   mu4e-attachment-dir "~/Downloads/Attachments"
   mu4e-change-filenames-when-moving t
   mu4e-headers-fields '((:maildir . 20) ;;(:account . 12)
                         (:human-date . 12)
                         (:flags . 4)
                         (:from . 25)
                         (:subject))
   mu4e-compose-reply-ignore-address
   (concat
    ;; "\\(?:no-?reply\\|.*\\.unwanted\\.domain\\.com\\|"
    "\\(?:no-?reply\\|"
    "holzner@wiiw\\.ac\\.a$\\)"
    )
   )
  ;;------------------------------------------------------------
  ;; Keybindings for saving attachments
  (map!
   :after mu4e
   :map mu4e-view-mode-map
   :ne "A" #'mu4e-view-mime-part-action)

  ;;------------------------------------------------------------
  ;; Function/keybinding to mark whole thread (e.g. for deletion)
  (map!
   :after mu4e
   :map (mu4e-headers-mode-map mu4e-view-mode-map)
   :n "t" #'mu4e-view-mark-thread
   ;; :n "ü" #'zauster--mu4e-message-mark-and-delete
   )


  ;;------------------------------------------------------------
  ;; Hide mail not from this context
  (setq mu4e-search-hide-predicate nil)


  ;;------------------------------------------------------------
  ;; enable mu4e-calendar
  (require 'mu4e-icalendar)
  (mu4e-icalendar-setup)

  (require 'org-agenda)
  (setq gnus-icalendar-org-capture-file "~/Dropbox/Org/mail.org")
  (setq gnus-icalendar-org-capture-headline '("Calendar"))
  (gnus-icalendar-org-setup)


  ;;------------------------------------------------------------
  ;; Warn me if external recipients are included!
  (defun zauster--mu4e-warn-external-recipients ()
    "Warn if any recipients are external to 'mycompany'."
    (interactive)
    (save-excursion
      (let* ((company-domain "wiiw\\.ac\\.at")  ;; Company domain pattern
             (to-recipients (message-field-value "To"))
             (cc-recipients (message-field-value "CC"))
             (all-recipients (if cc-recipients
                                 (concat to-recipients ", " cc-recipients)
                               to-recipients))
             (external-recipients nil)
             (internal-recipients nil))
        ;; Iterate over each recipient
        (if all-recipients
            (dolist (recipient (split-string all-recipients ",\\s-*"))
              (if (string-match company-domain recipient)
                  (setq internal-recipients t)
                (setq external-recipients t)))
          ;; Display appropriate warning based on recipient types
          (cond ((and external-recipients internal-recipients)
                 (if (y-or-n-p "Warning: Mix of internal and external recipients! Really continue? ")
                     (message "Email continued.")
                   (error "Email aborted.")))
                (external-recipients
                 (message "Only external recipients found."))
                (internal-recipients
                 (message "Only internal recipients found.")))))))

  (add-hook 'mu4e-compose-mode-hook 'zauster--mu4e-warn-external-recipients)
  (add-hook 'message-send-hook 'zauster--mu4e-warn-external-recipients)

  ;;------------------------------------------------------------
  ;; Warn me if there are multiple recipients when calling compose-reply!
  (defun zauster--mu4e-reply-warn-multiple-recipients ()
   "Warn if replying to multiple recipients and confirm if intended."
   (interactive)
   (save-excursion
     (let* ((to-recipients (message-field-value "To"))
            (cc-recipients (message-field-value "CC"))
            (all-recipients (if cc-recipients
                                (concat to-recipients ", " cc-recipients)
                              to-recipients))
            (recipients-list (split-string all-recipients ",\\s-*")))
       ;; Debugging line - display the list of recipients
       ;; (message "Testing: %s" recipients-list)
       ;; Check if there's more than one recipient
       (when (> (length recipients-list) 1)
         (if (y-or-n-p "Multiple recipients found. Do you really want to reply to only one?")
             (message "Email continued.")
           (error "Email aborted."))))))

  (advice-add 'mu4e-compose-reply :before #'zauster--mu4e-reply-warn-multiple-recipients)
  ;; (advice-add 'mu4e-compose-reply-to :before #'zauster--mu4e-reply-warn-multiple-recipients)

  (defun mu4e--compose-before-send ()
    "Function called just before sending a message."
    ;; Remove References: if In-Reply-To: is missing.
    ;; This allows the user to effectively start a new message-thread by
    ;; removing the In-Reply-To header.
    (when (eq mu4e-compose-type 'reply)
      (unless (message-field-value "In-Reply-To")
        (message-remove-header "References")))
    (when use-hard-newlines
      (mu4e--send-harden-newlines))
    ;; in any case, make sure to save the message; this will also trigger
    ;; before/after save hooks, which fixes up various fields.
    (set-buffer-modified-p t)
    ;; (save-buffer) ;; removed due to Forked due to this issue https://github.com/jeremy-compostella/org-msg/issues/200
    ;; now handle what happens _after_ sending
    (add-hook 'message-sent-hook #'mu4e--compose-message-sent nil t))
  ) ;; end after! mu4e


;;------------------------------------------------------------
;; 4. Writing Mail
(after! org-msg ;; (mu4e org-msg)
  (setq
   ;; mu4e-compose-format-flowed nil
   org-msg-options "html-postamble:nil toc:nil author:nil email:nil \\n:t -:t ^:{} H:5 num:0"
   org-msg-greeting-fmt "\n\n"
   org-msg-default-alternatives '((new           . (text))
                                  (reply-to-html . (text html))
                                  (reply-to-text . (text)))
   org-msg-convert-citation t
   org-msg-signature nil)

  ;; ;; Move point to the message body after creating the message
  ;; (advice-add 'mu4e-compose-wide-reply :after #'org-msg-goto-body)
  ;; (advice-add 'mu4e-compose-reply :after #'org-msg-goto-body)
  ;; (advice-add 'mu4e-compose-reply-to :after #'org-msg-goto-body)
  ;; ;; (advice-add 'mu4e-compose-forward :after #'org-msg-goto-body) ;; anyway not good to start in body

   ;; (setq zauster--org-msg-signature
  (setq mu4e-compose-signature
         "
#+begin_signature
--- \\\\
Oliver Reiter \\\\
Economist @ wiiw \\\\
www.wiiw.ac.at \\\\
zauster.gitlab.io/blog \\\\
#+end_signature
")
  ;; Economist @ wiiw
  ;; The Vienna Institute for International Economic Studies
  ;; T: +43 1 533 66 10-36
  ;; reiter@wiiw.ac.at

  (defun zauster--insert-signature-at-point ()
      "Function to insert signature at current point position"
    (interactive)
    (save-excursion
      (insert mu4e-compose-signature)))

  (map!
   :after (mu4e org-msg)
   :map (mu4e-compose-mode-map org-msg-edit-mode-map)
   :localleader
   "m g b" #'org-msg-goto-body
   "m g c" #'message-goto-cc
   "m g f" #'message-goto-from
   "m g t" #'message-goto-to
   "m g s" #'message-goto-subject
   "m s" #'zauster--insert-signature-at-point
   )

  ) ;; end after! org-msg

(use-package! consult-mu
  :after (mu4e consult))
