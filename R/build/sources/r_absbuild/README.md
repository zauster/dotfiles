
Arch Build System (ABS) PKGBUILD for R
====

My patch and update script for building the package _R_.

Run

```
./update.sh
```

to get the newest `PKGBUILD` from the ABS tree, patch the
`PKGBUILD` and build it.

Requires
- asp
- git
to be installed

`relinkBLAS` is defined as below in the `.bashprofile`

```

relinkRBLAS() {
   cd /usr/lib/R/lib/
   sudo rm -f libRblas.so.backup libRlapack.so.backup
   sudo mv libRblas.so libRblas.so.backup
   sudo mv libRlapack.so libRlapack.so.backup
   sudo ln -s /usr/lib/libblas.so libRblas.so
   sudo ln -s /usr/lib/liblapack.so libRlapack.so
   cd -
}

```
