
;;------------------------------------------------------------
;; Org mode config

;; auto-save all org buffers after changes to an .org-file
;; https://emacs.stackexchange.com/questions/21754/how-to-automatically-save-all-org-files-after-marking-a-repeating-item-as-done-i
;; https://www.reddit.com/r/emacs/comments/8dh9ta/auto_save_org_file_when_clocking_changing_agenda/
(defmacro zauster--ignore-arg-and-call (fnc)
  "Return function that ignores its arguments and invokes FNC."
  `(lambda (&rest _rest)
     (funcall ,fnc)))


;; Supposedly these configs have to be set before org
;; loads...
;; Set to the files (or directory of files) you want sync'd
(setq org-agenda-files (quote ("~/Dropbox/Org"))
      org-ref-notes-directory "~/Dropbox/Org/roam")

(after! org

  ;; Set that the agenda is shown maximized
  (setq org-agenda-window-setup 'only-window
        ;; org files should be opened with headings folded in
        org-startup-folded t)

  ;; Set org-refile targets as all agenda files
  (setq org-refile-use-outline-path 'file)
  ;; Do not complete in steps, does not work with helm
  (setq org-outline-path-complete-in-steps nil)
  ;; Ask before refiling under newly created header
  (setq org-refile-allow-creating-parent-nodes 'confirm)
  ;; First element for refiling is the current file, the second
  ;; element or other files. For both I use only the first two
  ;; levels of headlines for refiling
  (setq org-refile-targets
        '((nil :maxlevel . 2)
          (org-agenda-files :maxlevel . 2)))
  ;; For future: if a lot of heading, caching may help
  ;; (setq org-refile-use-cache t)


  ;; Set some org-agenda related settings
  (setq
   ;; Warn me of any deadlines in next 7 days
   org-deadline-warning-days 7
   ;; Always start Agenda on Monday
   org-agenda-start-on-weekday '1
   org-agenda-start-day "-0d"
   ;; Show me tasks scheduled or due in current week (or fortnight)
   org-agenda-span 'week ;; or 'fortnight
   ;; Don't show tasks as scheduled if they are already shown as a
   ;; deadline
   org-agenda-skip-scheduled-if-deadline-is-shown t
   ;; Don't give a warning colour to tasks with impending deadlines
   ;; if they are scheduled to be done
   org-agenda-skip-deadline-prewarning-if-scheduled
   (quote pre-scheduled)
   ;; Items with no date are displayed on top
   org-sort-agenda-notime-is-late t
   )

  ;; Capture mode, default notes file. Move everything to inbox and
  ;; refile from there
  (setq org-default-notes-file (concat org-directory "/inbox.org"))
  (setq +org-capture-projects-file "work_projects.org"
        +org-capture-notes-file "notes.org"
        +org-capture-todo-file "personal_todo.org")
  (setq org-contacts-files (concat org-directory "/contacts.org"))

  (setq org-capture-templates
        `(
         ;;----------------------------------------
         ;; Most general inbox, collection of everything
         ("i" "Inbox, refile later" entry
          (file "~/Dropbox/Org/inbox.org")
          "* %? \n\nAdded: %U"
          :empty-lines 1
          :prepend t)
         ("n" "Misc notes" entry
          (file+headline +org-capture-notes-file "General Notes")
          "* %? \n\nAdded: %U"
          :empty-lines 1
          :prepend t)
         ;; Notes about current setup and tips&tricks I
         ;; might need to remember for later
         ("l" "Linux notes" entry
          (file+headline +org-capture-notes-file "Linux Notes")
          "* %? :LINUX: \n\nAdded: %U"
          :empty-lines 1
          :prepend t)

         ;;----------------------------------------
         ;; Personal ToDos
         ("p" "Personal ToDos")
         ;; When Arztrechnungen should be filed with social
         ;; insurance
         ("pa" "Arztrechnung" entry
          (file+headline +org-capture-todo-file "Arztrechnung")
          ,(concat "* TODO Arztrechnung %? %u einreichen [/] :PERSONAL:RECHNUNG: \n"
                   "- [ ] GKK eingereicht \n"
                   "- [ ] Uniqa eingereicht \n"
                   "- [ ] Geld erhalten \n"
                   "Added: %U")
          :empty-lines 1
          :prepend t)
         ;; General personal TODO item
         ;; Nachwuchs-related TODOs
         ("pn" "Nachwuchs" entry
          (file+headline +org-capture-todo-file "Nachwuchs")
          "* TODO %? :ADELE: \n\nAdded: %U"
          :empty-lines 1
          :prepend t)
         ("pt" "Personal ToDo" entry
          (file+headline +org-capture-todo-file "Tasks")
          "* %? :PERSONAL: \n\nAdded: %U"
          :empty-lines 1
          :prepend t)

         ;;----------------------------------------
         ;; Mail capture templates
         ("m" "Email Workflow")
         ("mf" "Follow Up" entry (file+olp "~/Dropbox/Org/mail.org" "Follow Up")
          ,(concat "* TODO Follow up with %:fromname on %:subject \n"
                   "SCHEDULED:%^T \n"
                   "At: %a\n"
                   "%i")
          :immediate-finish t)
         ("mr" "Read Later" entry (file+olp "~/Dropbox/Org/mail.org" "Read Later")
          ,(concat "* TODO Read %:subject from %:fromname \n"
                   "SCHEDULED:%t \n"
                   "At: %a\n"
                   "%i")
          :immediate-finish t)

         ;;----------------------------------------
         ;; Org templates for work-related stuff
         ("w" "General work related")
         ;; File a meeting
         ("wm" "Meeting" entry
          (file "~/Dropbox/Org/work_meetings.org")
          "* SCHEDULED Meeting %? \nSCHEDULED: %^T \n\nAdded: %U\nAt: %a"
          :empty-lines 1
          :prepend t)
         ;; File a work ToDo, default: scheduled now
         ("wt" "ToDo" entry
          (file+headline "~/Dropbox/Org/work_todo.org" "Tasks")
          "* TODO %? :WORK:\nSCHEDULED: %T\n\nAdded: %U\nAt: %a"
          :empty-lines 1
          :prepend t)
         ;; File a work-related R-programming item, no given scheduled date
         ("wr" "R-programming ToDo" entry
          (file+headline "~/Dropbox/Org/work_todo.org" "R-programming")
          "* TODO %? :WORK:\n\nAdded: %U\nAt: %a"
          :empty-lines 1
          :prepend t)
         ;; File a work-related research idea
         ("wi" "Research idea" entry
          (file "~/Dropbox/Org/research_ideas.org")
          "* %? :IDEA:\n\nAdded: %U\nAt: %a"
          :empty-lines 1
          :prepend t)
         ;; Create a work contact entry
         ("wc" "Work contact" entry
          (file+headline "~/Dropbox/Org/contacts.org" "Work")
          ,(concat "* %^{org-contacts-template-name}"
                   "\n:PROPERTIES:"
                   "\n:Address: %^{Some address}"
                   "\n:Birthday: %^{yyyy-mm-dd}"
                   "\n:Email: %^{org-contacts-template-email}"
                   "\n:Note: %^{Note}"
                   "\n:END:")
          :prepend t)
         ;; File a entry for the Zeiterfassung
         ("wz" "Zeiterfassung" entry
          (file+olp+datetree "~/Dropbox/Org/work_todo.org")
          "* %? "
          :empty-lines 0)

         ;;----------------------------------------
         ;; Will use {org-directory}/{+org-capture-projects-file} and store
         ;; these under {ProjectName}/{Tasks,Notes,Changelog} headings. They
         ;; support `:parents' to specify what headings to put them under, e.g.
         ;; :parents ("Projects")
         ("wp" "Projects work related")
         ("wpt" "Project todo" entry
          (function +org-capture-central-project-todo-file)
          "* TODO %? :WORK:\n %i\nAt: %a"
          :heading "Tasks"
          :prepend t)
         ("wpn" "Project notes" entry
          (function +org-capture-central-project-notes-file)
          "* %U %? \n %i\nAt: %a\nLine number: %(with-current-buffer (org-capture-get :original-buffer) (number-to-string (line-number-at-pos)))"
          :heading "Notes")
         ("wpd" "Project documentation" entry
          (function +org-capture-central-project-notes-file)
          "* %U %? \n %i\nAt: %a"
          :heading "Documentation"
          :prepend t)
         ("wpm" "Project meetings" entry
          (function +org-capture-central-project-notes-file)
          "* %U %? :WORK:\n %i\nAt: %a"
          :heading "Meetings"
          :prepend t)
         ))

  ;; save org-files after each change
  (advice-add 'org-agenda-clock-in :after (zauster--ignore-arg-and-call #'org-save-all-org-buffers))
  (advice-add 'org-deadline        :after (zauster--ignore-arg-and-call #'org-save-all-org-buffers))
  (advice-add 'org-schedule        :after (zauster--ignore-arg-and-call #'org-save-all-org-buffers))
  (advice-add 'org-store-log-note  :after (zauster--ignore-arg-and-call #'org-save-all-org-buffers))
  (advice-add 'org-todo            :after (zauster--ignore-arg-and-call #'org-save-all-org-buffers))

  (map!
   :map org-mode-map
   :localleader
   ;; Use a more convenient way to mark items as done: , x
   :n "x" #'org-ctrl-c-ctrl-c
   ;; prefix for tables
   :prefix "b"
   :n "k" #'org-table-shrink
   )

  ;;------------------------------------------------------------
  (defun zauster--org-agenda-list ()
    "Open a new workspace, name it 'agenda' and open the org-agenda there"
    (interactive)
    (+workspace/new "agenda")
    (org-agenda-list)
    )

  (map!
   :leader
   ;; org-agenda remappings
   :prefix "o"
   "a a" #'zauster--org-agenda-list
   "a h" #'org-agenda
   )

  ;; beautify org-mode
  (setq org-hide-emphasis-markers t
        org-ellipsis " [...] ")

  (setq org-latex-to-mathml-convert-command
        "latexmlmath '%i' --presentationmathml=%o")

  (setq org-cite-csl-styles-dir "~/Zotero/styles")

  ;; prevents org-capture-mode to be activated, even after agenda has
  ;; been started
  ;; copied from https://github.com/doomemacs/doomemacs/issues/5714
  (defadvice! dan/+org--restart-mode-h-careful-restart (fn &rest args)
    :around #'+org--restart-mode-h
    (let ((old-org-capture-current-plist (and (bound-and-true-p org-capture-mode)
                                              (bound-and-true-p org-capture-current-plist))))
      (apply fn args)
      (when old-org-capture-current-plist
        (setq-local org-capture-current-plist old-org-capture-current-plist)
        (org-capture-mode +1))))

  ;; ;; If you get logs of error messages having to do with
  ;; ;; org-element--cache-sync, add this to your config:
  ;; (add-hook! 'org-capture-after-finalize-hook (org-element-cache-reset t))

  )                                     ;; end of after! org

;; ;; org export using pandoc
;; (after! ox-pandoc
;;   ;; set output options
;;   (setq org-pandoc-options-for-latex-pdf
;;         '((bibliography . "/home/reitero/Dropbox/Literature/Bibliography/orbib.bib")
;;           (citeproc . t)))
;;   (setq org-pandoc-options-for-odt
;;         '((bibliography . "/home/reitero/Dropbox/Literature/Bibliography/orbib.bib")
;;           (citeproc . t)))
;;   )


;; do a full cycle through the subtree: first only highest
;; level heading, then sub-headings, finally all children
(after! evil-org
  (remove-hook 'org-tab-first-hook
               #'+org-cycle-only-current-subtree-h))


(after! org-roam
  ;; see https://graphviz.org/documentation/ and
  ;; https://graphviz.org/pdf/dot.1.pdf for a doc on grapviz
  ;; and the fdp algo.
  (setq! org-roam-graph-extra-config
        '(("layout" . "fdp")
          ("overlap" . "false")
          ("K" . "1")))
  (setq! org-roam-graph-viewer
        "/usr/bin/imv")

  (defun zauster--org-roam-rg-search ()
    "Search org-roam directory using consult-ripgrep. With live-preview."
    ;; Copied from https://org-roam.discourse.group/t/using-consult-ripgrep-with-org-roam-for-searching-notes/1226/9
    (interactive)
    (let ((consult-ripgrep-command "rg --null --smart-case --type org --line-buffered --color=always --max-columns=500 --no-heading --line-number . -e ARG OPTS"))
      (consult-ripgrep org-roam-directory)))
  (map!
   :map doom-leader-notes-map
   :desc "Search org-roam dir"
   "r x" #'zauster--org-roam-rg-search
   )
  )
