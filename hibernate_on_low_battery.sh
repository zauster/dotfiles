#!/bin/bash

bat_dir="/sys/class/power_supply/BAT0"

if test -f "${bat_dir}/status"; then
    bat_status=$(cat $bat_dir/status)
    bat_capacity=$(cat $bat_dir/capacity)
    if [ "${bat_status}" = "Discharging" ]; then

        if (( "${bat_capacity}" <= 5)); then
            notify-send -u critical "Hibernating now" " See you in a while "
            # printf "Hibernating now! ${bat_capacity}"
            /usr/bin/systemctl hibernate
        elif (( "${bat_capacity}" <= 10)); then
            message="Mode: ${bat_status} \n Capacity: ${bat_capacity}%\n"
            # printf "Shutdown immediate! ${bat_capacity}"
            notify-send -u critical "WARNING: Shutdown immediate" "${message}"
        elif (( "${bat_capacity}" <= 15)); then
            message=" Mode: ${bat_status} \n Capacity: ${bat_capacity}%\n"
            # printf "Need for recharging! ${bat_capacity}"
            notify-send -u normal "Need for recharging" "${message}"
        else
            ## do nothing
            # printf "Enough capacity left."
            :
        fi

    elif [ "${bat_status}" = "Charging" ]; then
        # printf "Battery is charging"
        :
    fi
else
    ## no file, thus on AC
    # printf "Laptop on AC"
    :
fi
