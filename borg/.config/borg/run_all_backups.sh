#!/bin/sh

server=192.168.0.206
port=22                 # port
connect_timeout=5       # Connection timeout

timeout $connect_timeout bash -c "</dev/tcp/$server/$port"
if [ $? == 0 ];then
   echo -e "\n=> SSH Connection to raspberry pi at $server over port $port is possible, starting backup"
   # echo "$(pwd)"

   ## carry out the first backup of the work data, to the
   ## Toshiba external drive
   echo -e "\n=> Backing up to Toshiba ..."
   export BORG_REPO=reitero@rpi:/mnt/backup_tosh/backup_Arbeit
   export BORG_PASSCOMMAND='gpg2 --quiet --for-your-eyes-only --no-tty --decrypt /home/reitero/.config/borg/borg-toshiba-passphrase.gpg'
   /bin/sh /home/reitero/.config/borg/run_backup_work.sh

   ## and the second backup of the work data, to the Western
   ## Digital external drive
   echo -e "\n=> Backing up to Western Digital ..."
   export BORG_REPO=reitero@rpi:/mnt/backup_wd/backup_Arbeit
   export BORG_PASSCOMMAND='gpg2 --quiet --for-your-eyes-only --no-tty --decrypt /home/reitero/.config/borg/borg-wd-passphrase.gpg'
   /bin/sh /home/reitero/.config/borg/run_backup_work.sh

else
   echo -e "\n=> SSH connection to raspberry pi at $server over port $port is *not* possible"
fi

