;;; +octave.el --- Some octave modifications -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Oliver Reiter
;;
;; Author: Oliver Reiter <https://github.com/zauster>
;; Maintainer: Oliver Reiter <oliver.reiter@snapdragon.cc>
;; Created: August 23, 2021
;; Modified: August 23, 2021
;; Version: 0.0.1
;; Keywords: octave
;; Homepage: https://github.com/zauster/+octave
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Some octave modifications
;;
;;; Code:

;;------------------------------------------------------------
;; Octave mode

;; Remove the assocation of objc-mode to '.m' files and add
;; only the association to octave-mode
;; https://stackoverflow.com/questions/51117964/how-to-remove-an-item-from-auto-mode-alist-emacs
(setq auto-mode-alist (delete '("\\.m\\'" . objc-mode) auto-mode-alist))
(setq auto-mode-alist (delete '("\\.m\\'" . octave-maybe-mode) auto-mode-alist))
(add-to-list 'auto-mode-alist '("\\.m\\'" . octave-mode))

(after! octave
  (setq octave-send-line-auto-forward t
        octave-comment-start "% "
        octave-block-comment-start "%% ")
  (setq inferior-octave-prompt ">> ")

  ;; /usr/share/emacs/28.0.50/lisp/progmodes/octave.el
  ;; [[file:/usr/share/emacs/28.0.50/lisp/progmodes/octave.el::defun octave-send-region (beg end][Octave buffer]]
  ;; override the `octave-send-region` function to always
  ;; move the point to the end of the buffer
  (defun octave-send-region (beg end)
    "Send current region to the inferior Octave process."
    (interactive "r")
    (inferior-octave t)
    (let ((proc inferior-octave-process)
          (string (buffer-substring-no-properties beg end)))
      (save-excursion
        (with-current-buffer inferior-octave-buffer
          ;; https://lists.gnu.org/r/emacs-devel/2013-10/msg00095.html
          (compilation-forget-errors)
          (set-window-point (get-buffer-window (current-buffer)) (point-max))
          (goto-char (point-max))
          ;; (message (format "point at %d" (point)))
          (insert-before-markers string "\n")
          (comint-send-string proc (concat string "\n")))
        )
      (deactivate-mark))
    (if octave-send-show-buffer
        (display-buffer inferior-octave-buffer)))


  (map! :map octave-mode-map
        "M-RET" #'octave-indent-new-comment-line
        :localleader
        :prefix "e"
        "l" #'octave-send-line
        "a" #'octave-send-block
        "b" #'octave-send-buffer
        "f" #'octave-send-defun
        "r" #'octave-send-region)

  (set-popup-rule! "^\\*Inferior Octave\\*$" ;;
    :quit nil
    :side 'right
    :width .5
    :slot -1
    ;; :vslot -4
    :vslot 0
    :ttl nil
    :modeline t)

  )


;; (provide '+octave)
;; ;;; +octave.el ends here
