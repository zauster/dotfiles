;;; ../../dotfiles/doom/.config/doom/+zauster.el -*- lexical-binding: t; -*-


;;------------------------------------------------------------
;; Function to open a file in dired, using an external app rather than
;; some inbuilt method. Use e.g. for opening PDFs with evince.
(defun zauster--open-in-external-app (&optional file)
  "Open the current file or dired marked files in external app.

The app is chosen from your OS's preference. Copied from
`xah-open-in-external-app', mentioned in
https://stackoverflow.com/questions/25109968/in-emacs-how-to-open-file-in-external-program-without-errors"
  (interactive)
  (let (doIt
        (myFileList
         (cond
          ((string-equal major-mode "dired-mode") (dired-get-marked-files))
          ((not file) (list (buffer-file-name)))
          (file (list file)))))

    (setq doIt (if (<= (length myFileList) 5)
                   t
                 (y-or-n-p "Open more than 5 files? ")))

    ;; (when doIt
    ;;   (cond
    ;;    ((string-equal system-type "windows-nt")
    ;;     (mapc (lambda (fPath) (w32-shell-execute "open" (replace-regexp-in-string "/" "\\" fPath t t))) myFileList))
    ;;    ((string-equal system-type "darwin")
    ;;     (mapc (lambda (fPath) (shell-command (format "open \"%s\"" fPath))) myFileList))
    ;;    ((string-equal system-type "gnu/linux")
    ;;     (mapc (lambda (fPath) (let ((process-connection-type nil)) (start-process "" nil "xdg-open" fPath))) myFileList))))

    ;; TODO Open the file using `dired-guess-shell-alist-user' values
    ;; https://emacs.stackexchange.com/questions/3105/how-to-use-an-external-program-as-the-default-way-to-open-pdfs-from-emacs

    (when doIt
      (mapc (lambda (fPath) (let ((process-connection-type nil))
                         (start-process "" nil "xdg-open" fPath))) myFileList))
    ))


;;------------------------------------------------------------
;; Function to insert filename of current buffer
(defun zauster--insert-file-name (filename &optional args)
  "Insert name of file FILENAME into buffer after point.

  Prefixed with \\[universal-argument], expand the file name to
  its fully canocalized path.  See `expand-file-name'.

  Prefixed with \\[negative-argument], use relative path to file
  name from current directory, `default-directory'.  See
  `file-relative-name'.

  The default with no prefix is to insert the file name exactly as
  it appears in the minibuffer prompt."
  ;; Based on insert-file in Emacs -- ashawley 20080926
  (interactive "*fInsert file name: \nP")
  (cond ((eq '- args)
         (insert (file-relative-name filename)))
        ((not (null args))
         (insert (expand-file-name filename)))
        (t
         (insert (file-name-nondirectory filename)))))


;;------------------------------------------------------------
;; go to the notes of the current project
(defun zauster--goto-org-notes-current-project ()
  "Open the project org-file and go to the heading of the
current project"
  (interactive)
  (let ((current-project-name (projectile-project-name))
        (project-file-path (expand-file-name +org-capture-projects-file org-directory)))
    (message "Current project: %s" current-project-name)
    (message "Project file: %s" project-file-path)
    (switch-to-buffer
     (or (get-file-buffer project-file-path)
         (find-file project-file-path)))
    (goto-char
     (org-find-exact-headline-in-buffer current-project-name))
    (org-fold-show-subtree)))
;; (set-window-point
;;  (get-buffer-window (current-buffer))
;;  (org-find-exact-headline-in-buffer
;;   (projectile-project-name)
;;   (current-buffer)))



;;------------------------------------------------------------
;; Map keys to the defined functions
(map!
 :leader
 :prefix "z"
 "i" #'zauster--insert-file-name
 "n" #'zauster--goto-org-notes-current-project
 "o" #'zauster--open-in-external-app
 )
