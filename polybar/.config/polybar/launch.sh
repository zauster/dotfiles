#!/usr/bin/env sh

# # Terminate already running bar instances
# killall -q polybar

# # Wait until the processes have been shut down
# while pgrep -x polybar >/dev/null; do sleep 1; done

# # Launch bar
# # polybar zauster &

# if type "xrandr"; then
#   for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
#     MONITOR=$m polybar --reload zauster &
#   done
# else
#   polybar --reload zauster &
# fi




# #!/bin/bash
(
  flock 200

  killall -q polybar

  while pgrep -u $UID -x polybar > /dev/null; do sleep 0.5; done

  outputs=$(xrandr --query | grep " connected" | cut -d" " -f1)
  tray_output=HDMI1

  for m in $outputs; do
    if [[ $m == "DVI-D-1" ]]; then
        tray_output=$m
    fi
    if [[ $m == "eDP1" ]]; then
        tray_output=$m
    fi
  done

  for m in $outputs; do
    export MONITOR=$m
    export TRAY_POSITION=none
    if [[ $m == $tray_output ]]; then
      TRAY_POSITION=right
    fi

    polybar --reload zauster </dev/null >/var/tmp/polybar-$m.log 2>&1 200>&- &
    disown
  done
) 200>/var/tmp/polybar-launch.lock


echo "Bars launched..."
