;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Oliver Reiter"
      user-mail-address "oliver.reiter@snapdragon.cc")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))
(setq doom-font (font-spec :family "Fira Code" :size 16)
      doom-big-font (font-spec :family "Fira Code" :size 30)
      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 16)
      doom-serif-font (font-spec :family "Merriweather" :weight 'light)
      )

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'modus-operandi) ;; was gruvbox

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Dropbox/Org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'relative)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.


;;============================================================


;;------------------------------------------------------------
;; General config

;; Subword motion for camelCase words
(global-subword-mode 1)

;; variables to be set
(setq
 ;; auto-save my files
 auto-save-default t

 ;; use backup files
 make-backup-files t

 ;; while in insert mode, all changes are
 ;; considered one chunk. This settings makes it
 ;; more granular
 evil-want-fine-undo t

 ;; Set the local leader to , as in spacemacs
 evil-snipe-override-evil-repeat-keys nil
 doom-localleader-key ","

 ;; use the scratch buffer as lisp interaction buffer
 doom-scratch-initial-major-mode 'lisp-interaction-mode

 ;; ;; disable primary selection, enable kill ring
 ;; select-enable-primary t
 )

;; authentication sources
(setq
 auth-sources '("~/.config/emacs/.local/etc/authinfo.gpg"))

;; Avy settings
(setq
 ;; avy key selection: use Neo v2 home row
 avy-keys '(?e ?n ?a ?r ?i ?t ?u ?d ?o ?s)
 ;; make avy use all windows by default
 avy-all-windows t)

(setq
 ;; The scope of evil-snipe should be the entire visible buffer.
 evil-snipe-scope 'line ;; was 'whole-visible
 show-paren-context-when-offscreen t
 )

;; default variable values
(setq-default
 ;; set column width fill to 70, for better laptop use
 fill-column 70
 )

;; Let the calendar start on Mondays
(setq calendar-week-start-day 1)

;;------------------------------------------------------------
;; do not always ask if the module should be compiled
(after! vterm
  (setq vterm-always-compile-module t)
  ;; Improve speed when using in terminal.
  ;;     Ref: https://www.reddit.com/r/emacs/comments/pjtm91/vterm_a_little_bit_slow/
  (setq vterm-timer-delay 0.01))

;;------------------------------------------------------------
;; some nicer movement keys in org
(map! (:after evil-org
       :map evil-org-mode-map
       :n "gk" (cmd! (if (org-on-heading-p)
                         (org-backward-element)
                       (evil-previous-visual-line)))
       :n "gj" (cmd! (if (org-on-heading-p)
                         (org-forward-element)
                       (evil-next-visual-line)))))


;;------------------------------------------------------------
;; Dired configuration
(after! dired
  (setq dired-listing-switches "-alhv"
        delete-by-moving-to-trash t)

  (map!
   :map dired-mode-map
   :n [DEL] #'dired-up-directory)
  )


;;------------------------------------------------------------
(map!
 :leader
 ;; anzu query replace is quite handy to have at a high level
 :prefix "s"
 "a" #'anzu-query-replace-regexp

 :prefix "c"
 "y" #'evilnc-copy-and-comment-lines

 :prefix "v"
 "v" #'er/expand-region
 )


;;------------------------------------------------------------
;; Evil motion keybindings
;; Switch h and l to be more coherent on my keyboard
(after! evil
  (map! :map evil-motion-state-map "h" #'evil-forward-char
        :map evil-motion-state-map "l" #'evil-backward-char
        ;; use ö ($) and ä (|) for moving to the end and
        ;; beginning of the line
        :map evil-motion-state-map "ö" #'evil-goto-column
        :map evil-motion-state-map "ä" #'evil-end-of-line)

  (map!
   :map evil-normal-state-map "C-M-+" #'doom/increase-font-size
   :map evil-normal-state-map "C-M-=" #'doom/reset-font-size
   :map evil-normal-state-map "C-M--" #'doom/decrease-font-size
   )

  ;; on my keyboard layout is 'h' on the right and 'l' on the left
  (map!
   :leader
   :prefix "w"
   "h" #'evil-window-right
   "l" #'evil-window-left
   "H" #'+evil/window-move-right
   "L" #'+evil/window-move-left
   )

  ;; set the escape sequence to zb
  (setq-default evil-escape-key-sequence "bz")
  (setq evil-escape-unordered-key-sequence t)
  )

(after! consult
  (map!
   "M-y" #'consult-yank-from-kill-ring
   ))

(after! cal-move
  (map!
   :map calendar-mode-map :n "h" #'calendar-forward-day
   :map calendar-mode-map :n "l" #'calendar-backward-day
   ))

(after! iedit
  (setq iedit-toggle-key-default nil))

(after! evil-multiedit
  (map!
   :map evil-multiedit-state-map :n "C-n" #'evil-multiedit-next
   :map evil-multiedit-state-map :n "C-p" #'evil-multiedit-prev
   )
  )

(use-package! drag-stuff
  :defer t
  :init
  (map! "<M-up>"    #'drag-stuff-up
        "<M-down>"  #'drag-stuff-down
        "<M-left>"  #'drag-stuff-left
        "<M-right>" #'drag-stuff-right))

;; unbind C-l, to force me to use zz instead
;; https://github.com/doomemacs/doomemacs/issues/1797
(map! "C-l" nil)

;;------------------------------------------------------------
;; Arrayify function
;; https://news.ycombinator.com/item?id=22129636

;; Select strings that are separated by newlines, then call
;; this function.
(defun arrayify (start end quote)
  "Turn strings on newlines into a QUOTEd, comma-separated one-liner."
  (interactive "r\nMQuote: ")
  (let ((insertion
         (mapconcat
          (lambda (x) (format "%s%s%s" quote x quote))
          (split-string (buffer-substring start end)) ", ")))
    (delete-region start end)
    (insert insertion)))


;;------------------------------------------------------------
;; only show the encoding of the buffer if it is not
;; "UTF-8". Taken from
;; https://github.com/sunnyhasija/DOOMEmacs/blob/master/README.org
(defun doom-modeline-conditional-buffer-encoding ()
  (setq-local doom-modeline-buffer-encoding
              (unless (or (eq buffer-file-coding-system 'utf-8-unix)
                          (eq buffer-file-coding-system 'utf-8)))))
(add-hook 'after-change-major-mode-hook
          #'doom-modeline-conditional-buffer-encoding)


;;------------------------------------------------------------
;; Disable flycheck globally
(global-flycheck-mode -1)

;;------------------------------------------------------------
;; ESS Stata
(after! ess-stata-mode
  :defer t
  (setq inferior-STA-start-args ""
        ;; this is the path on rdev
        ;; inferior-STA-program "/usr/local/stata16/stata-mp")
        ;; this is the path on REITERLINUX
        inferior-STA-program "/usr/local/bin/stata-mp")
  )


;;------------------------------------------------------------
(setq +doom-quit-messages
      '("  ==>  WAIT! Have you done the ArgeData entry?   <== "))

;;------------------------------------------------------------
(use-package! pkgbuild-mode
  :defer t
  :init
  ;; (setq pkgbuild-update-sums-on-save nil)
  :config
  ;; (add-hook! 'pkgbuild-mode-hook
  ;;   (setq mode-name "PKGBUILD"
  ;;         mode-line-process nil))
  )

;; marginalia
(after! marginalia
  (setq marginalia-command-categories
        (assq-delete-all 'projectile-switch-project marginalia-command-categories))
  )

;; Configure directory extension.
(use-package! vertico-directory
  :after vertico
  :ensure nil
  ;; More convenient directory navigation commands
  :bind (:map vertico-map
              ("RET" . vertico-directory-enter)
              ("DEL" . vertico-directory-delete-char)
              ("M-DEL" . vertico-directory-delete-word))
  ;; Tidy shadowed file names
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))

;;------------------------------------------------------------
;; LSP settings
(with-eval-after-load 'lsp-mode
  (add-to-list 'lsp-file-watch-ignored-directories "[/\\\\]env\\'")
  )


;;------------------------------------------------------------
;; use dired-subtree to quickly insert the contents of a
;; subdirectory
(use-package! dired-subtree
  :defer t
  :ensure t
  :after dired
  :bind (:map dired-mode-map
              ("TAB" . dired-subtree-toggle)))


;;------------------------------------------------------------
;; Bibliography management
(after! citar
  (setq! citar-bibliography '("/home/reitero/Dropbox/Literature/Bibliography/orbib.bib"))
  (setq
   ;;citar-library-paths '("/path/to/library/files/")
   citar-notes-paths '("/home/reitero/Dropbox/Org/roam"))

  (map! :leader :prefix "n"
        "b" #'citar-insert-citation))


;;------------------------------------------------------------
;; leo.el
(use-package! leo
  :defer t
  :config
  (map!
   :leader
   :prefix "s" ;; ("l" . "lang") as new prefix with description
   :desc "translate at leo.org" "x" #'leo-translate-word
   )
 )

;;------------------------------------------------------------
;; Magit icons
(use-package! magit-file-icons
  :after magit
  :ensure t
  :init
  (magit-file-icons-mode 1)
  :custom
  ;; These are the default values:
  (magit-file-icons-enable-diff-file-section-icons t)
  (magit-file-icons-enable-untracked-icons t)
  (magit-file-icons-enable-diffstat-icons t))

;; ;;------------------------------------------------------------
;; ;; Ellama
;; (use-package! ellama
;;   :defer t
;;   :init
;;   (setopt ellama-language "English")
;;   (require 'llm-ollama)
;;   (setopt ellama-provider
;;           ;; (make-llm-ollama
;;           ;;  :chat-model "llama3.2:latest" ;; "llama3:latest"
;;           ;;  :embedding-model "llama3.2:latest" ;; "llama3:latest"
;;           ;;  )
;;           '(("llama3.2" . (make-llm-ollama
;;                           :chat-model "llama3.2:latest"
;;                           :embedding-model "llama3.2:latest"
;;                           ))
;;             ("llama3.1" . (make-llm-ollama
;;                           :chat-model "llama3.1:latest"
;;                           :embedding-model "llama3.1:latest"
;;                           )))))

;;------------------------------------------------------------
;; GPTel
(use-package! gptel
  :defer t
  :config
  ;; (setq! gptel-api-key "your key")
  (setq
   gptel-model "llama3.2" ;;":latest"
   gptel-backend (gptel-make-ollama "Ollama"
                   :host "localhost:11434"
                   :stream t
                   :models '("llama3.2" "qwen2.5-coder"))
   gptel-default-mode 'org-mode)
  (map!
   :leader :prefix ("l" . "llm")
   "a" #'gptel-add
   "c" #'gptel
   "l" #'gptel-send
   "m" #'gptel-menu
   "x" #'gptel-abort))


(use-package! inline-diff
  :after gptel-rewrite) ;or use :defer

;; Updated version available at https://github.com/karthink/gptel/wiki
(use-package gptel-rewrite
  :after gptel
  :bind (:map gptel-rewrite-actions-map
         ("C-c C-i" . gptel--rewrite-inline-diff))
  :config
  (defun gptel--rewrite-inline-diff (&optional ovs)
    "Start an inline-diff session on OVS."
    (interactive (list (gptel--rewrite-overlay-at)))
    (unless (require 'inline-diff nil t)
      (user-error "Inline diffs require the inline-diff package."))
    (when-let* ((ov-buf (overlay-buffer (or (car-safe ovs) ovs)))
                ((buffer-live-p ov-buf)))
      (with-current-buffer ov-buf
        (cl-loop for ov in (ensure-list ovs)
                 for ov-beg = (overlay-start ov)
                 for ov-end = (overlay-end ov)
                 for response = (overlay-get ov 'gptel-rewrite)
                 do (delete-overlay ov)
                 (inline-diff-words
                  ov-beg ov-end response)))))
  (when (boundp 'gptel--rewrite-dispatch-actions)
    (add-to-list
     'gptel--rewrite-dispatch-actions '(?i "inline-diff")
     'append)))

;;------------------------------------------------------------
;; change l and h in pdf-view-mode
(after! pdf-view
  :config
  (map!
   :map pdf-view-mode-map :n "l" #'image-backward-hscroll
   :map pdf-view-mode-map :n "h" #'image-forward-hscroll
   ))

;;------------------------------------------------------------
;; add hook to python-mode to set fill column to a higher value
(add-hook! python-mode (setq fill-column 80))

;; Show trailing spaces.
(add-hook! prog-mode #'doom-enable-show-trailing-whitespace-h)

;; ;; Show fill column line.
;; (add-hook! prog-mode (display-fill-column-indicator-mode t))


;;------------------------------------------------------------
;; vterm on remote systems should use zsh by default
(after! vterm
  (add-to-list 'vterm-tramp-shells '("ssh" "/bin/zsh"))
  (add-to-list 'vterm-tramp-shells '("sshx" "/bin/zsh"))
  ;; (set-popup-rule! "*doom:vterm-popup:*" :size 0.35 :vslot -4 :select t :quit nil :ttl 0 :side 'right)
  )

;;------------------------------------------------------------
;; use conf-mode for systemd files
(add-to-list 'auto-mode-alist '("\\.scope\\'" . conf-mode))
(add-to-list 'auto-mode-alist '("\\.automount\\'" . conf-mode))
(add-to-list 'auto-mode-alist '("\\.device\\'" . conf-mode))
(add-to-list 'auto-mode-alist '("\\.mount\\'" . conf-mode))
(add-to-list 'auto-mode-alist '("\\.path\\'" . conf-mode))
(add-to-list 'auto-mode-alist '("\\.service\\'" . conf-mode))
(add-to-list 'auto-mode-alist '("\\.slice\\'" . conf-mode))
(add-to-list 'auto-mode-alist '("\\.socket\\'" . conf-mode))
(add-to-list 'auto-mode-alist '("\\.swap\\'" . conf-mode))
(add-to-list 'auto-mode-alist '("\\.target\\'" . conf-mode))
(add-to-list 'auto-mode-alist '("\\.timer\\'" . conf-mode))


;;------------------------------------------------------------
;; Casual: helper for calc
;; (use-package! casual-calc
;;   :after calc
;;   :config
;;   (define-key calc-mode-map (kbd "C-o") 'casual-calc-tmenu))


;;------------------------------------------------------------
;; sync-recentf
(use-package! sync-recentf
  :after recentf
  :init
  (recentf-mode 1)
  :custom
  ;; sync every X seconds:
  (setq recentf-auto-cleanup 30)
  ;; do not keep track of org-files, they are loaded way to often
  (dolist (item '("Mail/" "~/Mail/"
                  ".emacs.d/.local/" ".config/emacs/.local/straight"
                  "Dropbox/Org/roam/"
                  "@wiiw.ac.at" "pacmerge"
                  "workspaces/autosave" "etc/bookmarks"
                  "sync-recentf-marker" "/tmp/emacs-everywhere-*-chromium"))
    (add-to-list 'recentf-exclude item))
  )

;;------------------------------------------------------------
;; Breadcrumb for not loosing track where we are in code
(use-package! breadcrumb
  ;; :init
  ;; (breadcrumb-mode)
  :hook
  ((prog-mode) . breadcrumb-local-mode)
  )

;; ;;------------------------------------------------------------
;; ;; Indent-bars - now included in doom by default
;; ;; https://github.com/jdtsmith/indent-bars
;; ;; https://github.com/jdtsmith/indent-bars/blob/main/examples.md
;; (after! indent-bars
;;   :config
;;   (setq
;;    ;; indent-bars-no-descend-lists t
;;    ;; indent-bars-no-descend-string t
;;    ;; indent-bars-display-on-blank-lines t
;;    ;; indent-bars-highlight-current-depth t
;;    )
;;   )

;;------------------------------------------------------------
;; Ready-player
(use-package! ready-player
  :after dired
  ;; :ensure t
  :config
  (ready-player-mode +1)
  (map!
   ;; :map dired-mode-map
   :leader :prefix ("m" . "music")
   "m" #'ready-player-play
   "l" #'ready-player-load-dired-playback-buffer
   "s" #'ready-player-stop
   "t" #'ready-player-toggle-play-stop
   "n" #'ready-player-next
   "p" #'ready-player-previous
   ))

;;------------------------------------------------------------
;; load configuration files
(load! "+org")
(load! "+ess")
;; (load! "+recentf")
(load! "+mail")
(load! "+octave")
(load! "+julia")
(load! "+zauster")
