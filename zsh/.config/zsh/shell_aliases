#!/bin/bash

########################
##### Some aliases #####
########################

# enable color support of ls and also add handy aliases
if [ "$TERM" != "dumb" ]; then
    eval "`dircolors -b`"
    alias ls='ls --color=auto'
    alias dir='ls --color=auto --format=C'
    alias grep='grep --color=auto'
    #alias vdir='ls --color=auto --format=long'
fi

alias shutdown='sudo shutdown -h now'
alias reboot='sudo reboot'

alias ll='ls -lh'
alias la='ls -alh'
alias l='ls -CF'
# alias cx='cd $(fd --type d --hidden --exclude .git --exclude node_module --exclude .cache --exclude .npm --exclude .mozilla --exclude .meteor --exclude .nv | fzf)'
alias cx='cd $(fd --type d --exclude node_module | fzf)'

alias hg='history | grep'
alias pag='ps aux | grep'

# alias initLeftDisplay='xrandr --output HDMI1 --mode 1280x720 --left-of eDP1'
# alias shutLeftDisplay='xrandr --output HDMI1 --off'
# alias initRightTV='xrandr --output HDMI1 --auto --right-of eDP1'
alias open='xdg-open'


##------------------------------------------------------------
## git aliases ###
alias gst='git status'
alias gl='git pull'
alias gp='git push'
alias gd='git diff'
alias gc='git commit -v'
alias gca='git commit -v -a'
alias gb='git branch'
alias gba='git branch -a'
alias gco='git checkout'



alias R='R --no-save'


###
### Pacman alias
###

# Synchronize with repositories before upgrading packages that are out of date on the local system.
alias pacupg='yay -Syu --devel'

# Install specific package(s) from the repositories
alias pacin='yay -S'

# Install specific package not from the repositories but from a file
alias pacins='yay -U'

# Remove the specified package(s), retaining its configuration(s) and required dependencies
alias pacre='yay -R'

# Remove the specified package(s), its configuration(s) and unneeded dependencies
alias pacrem='yay -Rns'

# Display information about a given package in the repositories
alias pacrep='yay -Si'

# Search for package(s) in the repositories and the AUR
alias pacs='yay -Ss'

# Search for package(s) in _only_ the repositories
alias pacso='yay -Sso'

# Search for package(s) in _only_ the AUR
alias pacsa='yay -Ssa'

# Display information about a given package in the local database
alias pacloc='yay -Qi'

# Search for package(s) in the local database
alias paclocs='yay -Qs'

# Clean the local cache from old packages
alias pacclean='yay -Sc'

# Update and refresh the local package and ABS databases against repositories
alias pacupd='yay -Sy'

# Install given package(s) as dependencies of another package
alias pacinsd='yay -S --asdeps'

# Force refresh of all package lists after updating /etc/pacman.d/mirrorlist
alias pacmir='yay -Syy'

# Removes unneeded packages
alias pacro='yay -Rs $(yay -Qtdq)'

# alias pacrox="yay -Qtdq > /dev/null && yay -Rns \$(yay -Qtdq | sed -e ':a;N;$!ba;s/\n/ /g')"


## Power management aliases
alias reboot='sudo systemctl reboot'
alias poweroff='sudo systemctl poweroff'


## Doom Emacs alias
alias emacsen='emacsen -nw'
alias ec='emacsclient'
alias ect='emacsclient -a "" -t'
alias ecc='emacsclient -a "" -n -c'

## systemctl aliases
alias sctl='systemctl'
alias ssctl='sudo systemctl'
alias jctl='journalctl'


#####################
#### aliases end ####
#####################
