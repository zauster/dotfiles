

;;------------------------------------------------------------
;; R + ESS config
(after! ess
  (setq
   ess-ask-for-ess-directory nil
   ess-ask-about-transfile nil
   ess-use-flymake nil
   ess-auto-width nil ;; auto: synchronize R's "width" option to the window width.
   ;; nil: if nil, install.packages works (output is displayed)
   ess-assign-list '(" <- " " |> " " %>% " " %<-% " ";")
   inferior-R-args "--no-restore-data --no-save" ;; command line parameters when starting R
   ;; the following functions recognizes a project if it is
   ;; a git-controlled directory
   ess-gen-proc-buffer-name-function 'ess-gen-proc-buffer-name:project-or-directory
   ess-can-eval-in-background t
   ess-use-ido nil
   ess-indent-with-fancy-comments nil
   ess-fancy-comments nil)

  (setq ess-R-font-lock-keywords '((ess-R-fl-keyword:keywords . t)
                                   (ess-R-fl-keyword:constants . t)
                                   (ess-R-fl-keyword:modifiers . t)
                                   (ess-R-fl-keyword:fun-defs . t)
                                   (ess-R-fl-keyword:assign-ops . t)
                                   (ess-R-fl-keyword:%op% . t)
                                   (ess-fl-keyword:fun-calls . t)
                                   (ess-fl-keyword:numbers . t)
                                   (ess-fl-keyword:operators . t)
                                   (ess-fl-keyword:delimiters . t)
                                   (ess-fl-keyword:= . t)
                                   (ess-R-fl-keyword:F&T . t)))

  (set-company-backend! 'inferior-ess-r-mode
    '(company-R-args company-R-objects company-dabbrev-code company-R-library :separate))

  (set-popup-rule! "^\\*R:*" ;; "^\\*R:*\\*" ;; "^\\*julia wilap\\*$"
    :quit nil
    :side 'right
    :width .5
    :slot -1
    :vslot 0
    :ttl nil
    :modeline t)

  ;;--------------------------------------------------
  ;; Some custom functions
  ;; start new R process and source current buffer
  (defun zauster--ess-restart-process-and-source-buffer ()
    "Restarts the R process and sources the current buffer"
    (interactive)
    (inferior-ess-reload)
    (ess-eval-buffer)
    )

  (defun zauster--ess-R-call-function-on-object (&optional fun)
    "Calls the R process with the given function FUN and object at point or the marked region.
If a region is selected, it evaluates FUN on the highlighted region;
otherwise, it uses the object at point."
    (interactive "MFunction to evaluate on object: ")
    (let ((ess-eval-visibly t)
          (object (if (use-region-p)
                      (buffer-substring-no-properties (region-beginning) (region-end))
                    (current-word))))
      (ess-eval-linewise (concat fun "(" object ")\n"))))

  ;; run `str' on the current object
  (defun zauster--ess-R-call-str-on-object ()
    "Get info for object at point by evaluating `str' on the object
in the inferior ESS process."
    (interactive)
    (zauster--ess-R-call-function-on-object "str")
    )


  ;;--------------------------------------------------
  ;; Some custom keybindings
  (map!
   (:after ess-help
    (:map ess-doc-map
     "s" #'zauster--ess-R-call-str-on-object
     "x" #'zauster--ess-R-call-function-on-object
     [C-return] #'ess-eval-region-or-line-visibly-and-step))
   :map ess-mode-map
   :n [C-return] #'ess-eval-region-or-line-visibly-and-step
   :i ";" #'ess-cycle-assign ;; only in insert mode (:i)
   :i "RET" #'ess-indent-new-comment-line ;; only in insert mode (:i)
   :localleader                           ;; "SPC m" or ,
   "a" #'ess-eval-region-or-function-or-paragraph
   "." #'ess-eval-paragraph-and-step
   :prefix "e"
   "a" #'ess-eval-region-or-function-or-paragraph
   "b" #'ess-eval-buffer
   "B" #'ess-eval-buffer-and-go
   "e" #'ess-eval-region-or-function-or-paragraph-and-step
   "f" #'ess-eval-function
   "F" #'ess-eval-function-and-go
   "l" #'ess-eval-line
   "L" #'ess-eval-line-and-go
   "r" #'ess-eval-region
   "R" #'ess-eval-region-and-go
   "x" #'zauster--ess-restart-process-and-source-buffer
   )

  (map!
   :map inferior-ess-mode-map
   :i ";" #'ess-cycle-assign ;; only in insert mode (:i)
   :i [M-up] #'comint-previous-matching-input-from-input
   :i [M-down] #'comint-next-matching-input-from-input
   )

  )


(after! ess-r-mode
  ;; (visual-line-mode -1)
  ;; disable this list of symbols because I don't like the
  ;; short arrow
  (setq ess-r-prettify-symbols nil)
  )

(add-hook! inferior-ess-mode (visual-line-mode +1))
