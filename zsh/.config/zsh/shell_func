#!/bin/bash

###
### Custom bash functions
###

## extract files
ex () {
       if [ -f $1 ] ; then
           case $1 in
             *.tar.bz2)     tar xjfv $1                ;;
             *.tar.gz)      tar xzfv $1                ;;
             *.bz2)         bunzip2 $1                ;;
             *.rar)         rar x $1                  ;;
             *.gz)          gunzip $1                 ;;
             *.tar)         tar xfv $1                 ;;
             *.tbz2)        tar xjfv $1                ;;
             *.tgz)         tar xzfv $1                ;;
             *.zip)         7z x $1                  ;;
             *.Z)           uncompress $1             ;;
             *)             echo "'$1' cannot be extracted via extract()" ;;
           esac
       else
           echo "'$1' is not a valid file"
       fi
}


##
## aurbuild
##
aurbuild() {
       cd ~/build/sources
       packagename=$1
       ## wget https://aur.archlinux.org/packages/${packagename:0:2}/$1/$1.tar.gz || return 1
       ## https://aur.archlinux.org/cgit/aur.git/snapshot/r-mkl.tar.gz
       curl -O https://aur.archlinux.org/cgit/aur.git/snapshot/$1.tar.gz || return 1
       echo "Got package..."
       ex $1.tar.gz || return 1
       rm $1.tar.gz || return 1
       cd $1
       makepkg -s || return 1
}

# edit file with root privs
function E() {
       emacsclient -c -a emacs "/sudo:root@localhost:$1"
}


yapgc () {
       yay -Syu
       if yay -Qtdq
       then
           echo ":: Unused packages found ..."
           yay -Rs $(yay -Qtdq)
       else
           echo ":: No unused packages ..."
       fi
}

##
## R benchmarks
##
timeR () {
       R CMD BATCH Rbenchmarks.R $1
       sed '/^> / d' -i $1
       sed '/^+ / d' -i $1
}

##
## WIIW stuff
##
connectWIIW () {
       unset DISPLAY
       cd /home/reitero/Dropbox/wiiw/reiter-vpn/
       sudo openvpn \
           --config reiter.conf
       cd -
}

mountW () {
       sudo mount -t cifs //172.16.0.3/W/ \
           /home/reitero/Arbeit/mnt/W \
           -o username=reiter,file_mode=0777,dir_mode=0777
}

mountP () {
       sudo mount -t cifs //172.16.0.3/P/Reiter/ \
           /home/reitero/Arbeit/mnt/P \
           -o username=reiter,file_mode=0777,dir_mode=0777
}

mountPfull () {
       sudo mount -t cifs //172.16.0.3/P/ \
           /home/reitero/Arbeit/mnt/P_full \
           -o username=reiter,file_mode=0777,dir_mode=0777
}

## when the directory is in the home directory
function sshfs_wiiw_home () {
       echo "Mounting project folder: $1"
       local MOUNTPOINT=/home/reitero/Arbeit/mnt/$1
       mkdir -p $MOUNTPOINT
       sshfs wiiw:/home/reitero/$1 $MOUNTPOINT -C -o ServerAliveInterval=30 -o reconnect
       echo "Mounting successful. Unmount with 'fusermount3 -u $MOUNTPOINT'"
}

## when the directory is in the mnt/ directory
function sshfs_wiiw_mnt () {
       local REMOTE_DIR=$1
       local MOUNTPOINT=/home/reitero/Arbeit/mnt/$REMOTE_DIR
       echo "Mounting project folder: $REMOTE_DIR to $MOUNTPOINT"
       mkdir -p $MOUNTPOINT
       sshfs wiiw:/mnt/$REMOTE_DIR $MOUNTPOINT -C -o ServerAliveInterval=30 -o reconnect
       echo "Mounting successful. Unmount with 'fusermount3 -u $MOUNTPOINT'"
}


relinkRBLAS() {
       ## cd /usr/lib/R/lib/
       sudo rm -f /usr/lib/R/lib/libRblas.so.backup /usr/lib/R/lib/libRlapack.so.backup
       sudo mv /usr/lib/R/lib/libRblas.so /usr/lib/R/lib/libRblas.so.backup
       sudo mv /usr/lib/R/lib/libRlapack.so /usr/lib/R/lib/libRlapack.so.backup
       sudo ln -s /usr/lib/libblas.so /usr/lib/R/lib/libRblas.so
       sudo ln -s /usr/lib/liblapack.so /usr/lib/R/lib/libRlapack.so
       ## cd -
}

doomup() {
       cd ~/.config/emacs
       echo "> Checking out master branch ..."
       git checkout master
       echo "> Running 'doom upgrade' ..."
       doom upgrade --aot
       echo "> Checking out zauster branch ..."
       git checkout zauster
       echo "> Rebase zauster onto master ..."
       git rebase master
       cd -
}
